# links_prg

Desktop app that aims to transcribe links.prg wirrten in Visual FOX Pro in Python. 
Links.prg is a desktop app that is used for personal storage of URL in database. 

## Getting started

The projects has requirements.txt file which holds the dependencies of the project.

Python version: >3.7


Note: I recommend to use a local environment.

Creating the repo on your drive:
```
cd existing_repo
git clone https://gitlab.com/Tudor.I/links_prg.git
```

CMD:
```
python3 -m venv path/to/your/virtual/
cd your_virtual_env_dir/Scripts
activate
cd ..
cd ..

python3 -m pip install -r requirements.txt
```


##### SQL Server dependencies
This app uses Microsoft SQL Server 2019 as database storage. You must have
installed SQL Server and MSSMS - Microsoft SQL Server Management Studio on your device. 
- [ ] [Microsoft SQL Server 2019](https://www.microsoft.com/en-us/Download/details.aspx?id=101064)
- [ ] [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)


You must have a backup file with the data for the restoration.

We recommend to use Windows User Authentication when configuring the Server.

When the app is run, it will be required the name of the sever.

After the login to the server, you will be ask to select the .bak file for restoration.



## Version

The version vers1 is finished. All you should do is to run the app.

The version vers2 is under development.

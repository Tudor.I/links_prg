import datetime
import re, os, random
import PySimpleGUI as sg
import pyodbc
from pyodbc import OperationalError, InterfaceError, ProgrammingError

def connection_database(restore_database_notexists=False, server_name=None):
    if restore_database_notexists:
        connection_string = "Driver={ODBC Driver 17 for SQL Server};" \
                            f"Server={server_name};" \
                            "Trusted_Connection=yes;"
    else:
        connection_string = "Driver={ODBC Driver 17 for SQL Server};" \
                        f"Server=DESKTOP-VTKFRFL\SQLEXPRESS;" \
                        f"Database=links;" \
                        f"Trusted_Connection=yes;"
    try:
        return pyodbc.connect(connection_string)
    except OperationalError:
        return None
    except InterfaceError as e:
        string = 'Cannot open database "links" requested by the login'
        match = re.search(string, str(e))
        if match is not None:
            return "Restore database"
        else:
            print(e)


def SQL_Select(con):
    select_command = "SELECT * FROM dbo.Links;"
    try:
        data = con.execute(select_command)
        data = data.fetchall()
        total_rows = []
        for i in range(len(data)):
            total_rows.append(list((data[i])))
        return total_rows
    except ProgrammingError:
        return "Error"



def SQL_insert_up(entry_data, cnx, sort_count, up=False, down=False):
    """
    :param data: type List - values that needs to pe inserted into SQL Tabel
    :param cnx: cursor
    :param sort_count: type int - position of the entry SQL tabel
    :return: string - Success or Error
    """
    index = 0
    global COPY_FROM_EXISTING_TABLE, SELECT_TMP_NEW_TABLE
    DELEMITER = sort_count - 1
    for entry in entry_data:
        if entry == r"\n":
            entry_data[index] = ""
        elif entry[-2:] == r"\n":
            entry_data[index] = entry[:-2]
        index+=1
    INSERT_STRING = "INSERT INTO dbo.tmp VALUES ("
    for data in entry_data:
        INSERT_STRING += f"'{data}',"
    INSERT_STRING += f"{sort_count});"
    if up:
        SELECT_TMP_NEW_TABLE = sort_count
        COPY_FROM_EXISTING_TABLE = sort_count - 1
    elif down:
        SELECT_TMP_NEW_TABLE = sort_count + 1
        COPY_FROM_EXISTING_TABLE = sort_count
    TMP_TABEL_COMM = "USE links;" \
                     "DROP TABLE IF EXISTS dbo.tmp;" \
                     "SELECT * INTO dbo.tmp FROM dbo.Links WHERE SORT_COUNTER < %s;" % (SELECT_TMP_NEW_TABLE)
    TMP_INSERT_REST_DATA = f"INSERT INTO tmp SELECT * FROM dbo.Links WHERE SORT_COUNTER > %s;" % (COPY_FROM_EXISTING_TABLE)
    UPDATE_STRING = "DECLARE @SO int; SET @SO = %s; UPDATE dbo.tmp SET @SO = SORT_COUNTER = @SO + 1 WHERE SORT_COUNTER > %s;" % (DELEMITER,DELEMITER)
    DELETE_EX_TABEL = "DROP TABLE IF exists dbo.Links;"
    RENAME_TMP_TABEL = "EXEC sp_rename 'tmp', 'Links';"
    try:
        cnx.execute(TMP_TABEL_COMM)
        cnx.execute(INSERT_STRING)
        cnx.execute(TMP_INSERT_REST_DATA)
        cnx.execute(UPDATE_STRING)
        cnx.execute(DELETE_EX_TABEL)
        cnx.execute(RENAME_TMP_TABEL)
        cnx.commit()
        while cnx.nextset():
            pass
        return "Success"
    except ProgrammingError:
        return "Error"


def SQL_backup(connection, file_path):
    if os.path.isdir(file_path):
        DATE_NOW = datetime.datetime.now()
        NUMBER = random.randint(0, 10000000000)
        FILE_NAME = f"{DATE_NOW.year}_{DATE_NOW.month}_{DATE_NOW.day}_backup_links_{NUMBER}.bak"
        file_path = file_path + r"\'" + FILE_NAME
        file_path = file_path.replace("'", "")
        file_path = file_path.replace("C:/", r"C:\'")
        file_path = file_path.replace("'", "")
        BACKUP_COMMAND = f"BACKUP DATABASE links TO DISK = '{file_path}';"
        connection.execute(BACKUP_COMMAND)
        while connection.nextset():
            pass
        return "Success"
    else:
        return "Error"

def SQL_restore_database(cursor, file_path, restore=False):
    if os.path.isfile(file_path) and file_path.find(".bak"):
        OFFLINE_DATABASE = "ALTER DATABASE links SET OFFLINE WITH ROLLBACK IMMEDIATE;"
        DROP_DATABASE = "DROP DATABASE if exists links;"
        RESTORE_DATABASE = f"RESTORE DATABASE links FROM DISK = '{file_path}' WITH REPLACE;"
        USE_DATABASE = "USE links;"
        if not restore:
            cursor.execute(OFFLINE_DATABASE)
        cursor.execute(DROP_DATABASE)
        cursor.execute(RESTORE_DATABASE)
        cursor.commit()
        while cursor.nextset():
            pass
        cursor.execute(USE_DATABASE)
        cursor.commit()
        return "Success"
    else:
        return "Error"



def SQL_update(data, cursor, ndx):
    UPDATE_COMM = "UPDATE links SET "
    for key, value in data:
        if key == "USER":
            key = "USEER"
        UPDATE_COMM += "%s = '%s'," % (key, value.strip())
    UPDATE_COMM = UPDATE_COMM[:-1]
    UPDATE_COMM += " WHERE SORT_COUNTER = %s;" % (ndx)
    try:
        cursor.execute(UPDATE_COMM)
        cursor.commit()
        return "Success"
    except ProgrammingError:
        return "Error"

def SQL_delete(data, connection, multiple_values=False):
    global delete_position, update_position
    SQL_delete_command = ''
    if multiple_values:
        for value in data:
            SQL_delete_command += f"DELETE FROM dbo.Links WHERE SORT_COUNTER = {value[-1]};"
        update_position = data[0][-1]-1
    else:
        SQL_delete_command += f"DELETE FROM dbo.Links WHERE SORT_COUNTER = {data[-1]};"
        update_position = data[-1]-1
    SQL_update_counter = "DECLARE @SO int; SET @SO = %s; UPDATE dbo.Links SET @SO = SORT_COUNTER = @SO + 1 WHERE SORT_COUNTER > %s;" % (int(update_position), int(update_position))
    try:
        connection.execute(SQL_delete_command)
        connection.execute(SQL_update_counter)
        connection.commit()
        while connection.nextset():
            pass
        return "Success"
    except ProgrammingError:
        return "Error"

def SQL_reindexare(connection):
    INDEXARE_COMMAND = "DECLARE @ST int; SET @ST = 0; UPDATE dbo.Links SET @ST = SORT_COUNTER = @ST + 1;"
    connection.execute(INDEXARE_COMMAND)
    connection.commit()
    try:
        while connection.nextset():
            pass
        return "Success"
    except ProgrammingError:
        return "Error"

def search_engine(q, entries):
    src = re.search(r'\bOR\b', q)
    if src is not None:
        src = [r.strip() for r in q.split("OR", 1)]
    else:
        src = q
    results = []
    visited = {}
    counter = 0
    for entry in entries:
        for j in range(len(entry)):
            if isinstance(src, list):
                for s in src:
                    if re.search(str(s).lower(), str(entry[j]).lower()) is not None:
                        tm_entry = str(entry)
                        if tm_entry not in visited:
                            results.append(entry)
                            visited[tm_entry] = counter
                            counter += 1
            else:
                if re.search(str(src).lower(), str(entry[j]).lower()) is not None:
                    results.append(entry)
                    break
    return results

if __name__ == "__main__":
    pass
    a = r"askdkasmdksda\n"
    print(a, a[:-2], a[-2:] )


    """data_per_row = []
    total_rows = []
    for i in range(5):
        for j in range(24):
            data_per_row.append(j)
        total_rows.append(data_per_row)
        data_per_row = []

    #echivalent
    total_rows1 = [[j for j in range(24)] for i in range(5)]

    print(total_rows)
    print(total_rows1)"""




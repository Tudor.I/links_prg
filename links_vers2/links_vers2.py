""""

31/08/2021 - Tudor Ilade

Fisierul .py principal in care va fi rulata logica codului central.  Metodele si functiile adiacente, vor fi localizate
in folderul procedures.

Aplicatia pleaca de la codul scris de Bița Răzvan-Nicolae in data de 10/01/2021.

Se foloseste ca si baza de date, SQL Server Express, iar pentru aplicatie, se foloseste PySimpleGUI.
"""
import PySimpleGUI as sg
import webbrowser
import pyodbc
from procedures.sql_commands import *
import re

class TableCreation:
    def __init__(self, nr_of_cols = None):
        self.nr_of_cols = nr_of_cols
        self.headings = ['Numele coloanei', "Tipul Coloanei", "Permite caseta NULL"]
        self.choices = ['Integer', "Varchar(50)", "Float", "Date time"]
        self.layout_table = [
            [sg.Text("Creare tabel SQL")],
            [sg.Text(" "*100)],
        ]
        if nr_of_cols is None:
            for col in range(10):
                self.layout_table += [sg.Input()]




class TableDisplay:
    sg.theme('Light Gray1')
    QT_ENTER_KEY1 = 'special 16777220'
    QT_ENTER_KEY2 = 'special 16777221'
    INPUT_BACKGROUND = "white"
    TEXT_COLOR = "black"
    BACKGROUND_COLOR_TABLE = "grey93"
    ALTERNATE_COLOR = "grey88"
    def __init__(self, data):
        self.headings = ["STARS", "TITLE", "LINK", "FOLDER", "DATA", "TYPE", "TAGS",
                "TITLE_RO", "AUTHORS", "LINK_RO", "USER", "DURATION", "TIME",
                "LIMBA", "CHANNEL", "PATH","VIEWS", "EMAIL", "TELEFON", "VOCABULARY",
                "LANGUAGE", "DETAILS", "NO_DETAILS", "SEARCH", "SORT", "DATA_OPER", "SEL", "SORT_COUNT"]
        self.menu_bar = [['&Toolbar', ['&Table',["New table", "Change table"], '&Database', ["New database", "Change database"],'E&xit']],]
        self.table = sg.Table(values=data[:][:], headings=self.headings, max_col_width=80,
                    vertical_scroll_only= False,
                    col_widths=[7 for i in range(28)],
                    display_row_numbers=True,
                    justification='left',
                    num_rows=14,
                    selected_row_colors=("white", "black"),
                    alternating_row_color=TableDisplay.ALTERNATE_COLOR,
                    background_color=TableDisplay.BACKGROUND_COLOR_TABLE,
                    key='-TABEL-',
                    row_height=35,
                    enable_events=True,
                    )

        self.layout = [
                       [sg.Menu(self.menu_bar, background_color=("gray87"), text_color="black")],
                        [self.table],
                       [sg.Text("_"*1200)],
                       [sg.Button("Backup"), sg.Text(" "*10), sg.Button("Restore database")]
                       ]



    def search_window(self, data):
        """
        Functia de cautare in tabel dupa un entry dat. Daca se introduce OR, vor face cautari
        dupa cuvantul(Grupul de cuvinte) la stanga de OR si dreapta de OR, altfel se va cauta dupa
        exact match.
        :param data: type list - toate datele din tabel
        :return: O lista cu valorile gasite. Daca lista va fi goala, nimic nu va fi afisat.
        """
        string = "Se va cauta cuvantul pe toate coloanele.\nDefault, se cauta exact match.\nAdaugati OR intre cuvinte " \
                        "sau grupuri de cuvinte pentru a cauta.\n" \
                 "Este luata in considerare doar prima aparitie a lui OR."
        layout = [
            [sg.Text(string)],
            [sg.Text("_"*500)],
            [sg.Text("Cauta: "), sg.InputText(size=(36,1), key='q', background_color=TableDisplay.INPUT_BACKGROUND
                                              , text_color=TableDisplay.TEXT_COLOR), sg.Yes("Cauta"), sg.Text(""),sg.Text(""),
             sg.Cancel("Anuleaza")],
            ]
        window = sg.Window("Cauta", layout, return_keyboard_events=True, size=(500,150))
        while True:
            event, values = window.read()
            if event == "Anuleaza" or event == sg.WINDOW_CLOSED:
                window.close()
                break
            if event == "Cauta":
                window.close()
                return search_engine(values['q'], data)

    def open_link(self, entry):
        """
        Functie raspunzatoare de deschiderea de link-uri din tabel.
        :param entry: type list - o lista care contine toate datele despre link-ul care urmeaza a fi accesat
        :return: invalid http daca valoarea nu corespunde unui link vali. Altfel, deschide link-ul.
        """
        if entry[1].find("http") != -1 and entry[2].find("http") == -1:
            webbrowser.open_new(entry[1])
        elif entry[1].find("http") == -1 and entry[2].find("http") != -1:
            webbrowser.open_new(entry[2])
        else:
            return "invalid http"



    def insert_window(self,sort_count,cursor,up=False, down=False):
        """Functie care insereaza datele in SQL. Dupa caz, poate fi deasupra (up) sau dedesubtul inregistrarii (down). Se face
        referire la index-ul din tabel de pe coloana SORT_COUNTER
        :param: sort_count - type int - positia inregistrarii din SQL tabel
        :param: up - Type bool - Daca este True, inregistrarea se va face la index-ul SORT_COUNT - 1
        :param: down - type Bool - Daca este True, inregistrarea se va face la index-ul SORT_COUNT + 1"""
        middle = []
        j = 0
        headings = self.headings[:-1]
        for i in range(len(self.headings)//2):
            if self.headings[j+1] == "SORT_COUNT":
                middle += [[sg.Text(self.headings[j], size=(14, 1)), sg.Multiline(key=self.headings[j], size=(38,1),
                                                                                  background_color=TableDisplay.INPUT_BACKGROUND,
                                                                                  text_color=TableDisplay.TEXT_COLOR),
                        ]]
            else:
                middle += [[sg.Text(self.headings[j], size=(14, 1)), sg.Multiline(key=self.headings[j], size=(38,1),
                                                                                  background_color=TableDisplay.INPUT_BACKGROUND,
                                                                                  text_color=TableDisplay.TEXT_COLOR),
                           sg.Text(self.headings[j+1],size=(14, 1)), sg.Multiline(key=self.headings[j+1], size=(38,1),
                                                                                  background_color=TableDisplay.INPUT_BACKGROUND,
                                                                                  text_color=TableDisplay.TEXT_COLOR
                                                                                  )]]
            j+=2
        layout = [
            [sg.Text(" "*60 + "Introdu o noua inregistrare", font=("Times New Roman", 15))],
            [sg.Text("_"*300)],
            [sg.Column(middle, scrollable=True)],
            [sg.Text("_" * 300)],
            [sg.Button("Adauga"), sg.Button("Anuleaza")]
        ]
        window = sg.Window("New entry",layout, size=(900, 550),return_keyboard_events=True, font=("Times New Roman", 11))
        while True:
            events, values = window.read()
            if events == "Adauga":
                new_entry = [values[i] for i in headings]
                if up:
                    window.close()
                    return SQL_insert_up(sort_count=sort_count, entry_data=new_entry, cnx=cursor, up=True)
                elif down:
                    window.close()
                    return SQL_insert_up(sort_count=sort_count, entry_data=new_entry, cnx=cursor, down=True)
            if events == "Anuleaza" or events == sg.WINDOW_CLOSED:
                window.close()
                return "Closed"


    def backup(self, cnx):
        """
        Functie responsabila de crearea de backup a tabelului existent in folderul de destinatie ales.
        :param cnx: varabila ce are stocata conexiunea cu baza de date
        :return: Nu returneaza nimic
        """
        cnx.autocommit = True
        cursor = cnx.cursor()
        layout = [
            [sg.Text("Selectati folderul de destinatie pentru backup")],
            [sg.Input(key="-FILE_PATH-",background_color=TableDisplay.INPUT_BACKGROUND,
                                        text_color=TableDisplay.TEXT_COLOR), sg.FolderBrowse()],
            [sg.Button("Creaza backup"), sg.Button("Anuleaza")]
        ]
        window = sg.Window("Backup", layout)
        while True:
            event, values = window.read()
            if event == "Creaza backup":
                if values['-FILE_PATH-']:
                    res =  SQL_backup(cursor, values['-FILE_PATH-'])
                    if res == "Success":
                        sg.popup("Backup-ul s-a efectuat cu succes!", title="Backup")
                        cnx.autocommit = False
                        window.close()
                        break
                    else:
                        sg.popup("Backup-ul nu s-a efectuat!")
                else:
                    sg.popup("Introduceti directorul de locatie!", title="Error")
            if event == "Anuleaza" or event == sg.WINDOW_CLOSED:
                window.close()
                cnx.autocommit = False
                break

    @staticmethod
    def restore_database(connection=None, restore_database_notexists=False):
        """
        Functie resonsabila cu restaurarea bazei de date. Daca restre_Database_notexists este True, procesul de restaurare
        a bazei de date in absenta ei pe SQL Server este inceputa.
        :param connection: conexiunea cu baza de date. Daca nu exista, default este None
        :param restore_database_notexists: type boolean - Daca valoarea este True, inseamna ca incearca sa se acceseze
        o baza de date care nu exista in SQL Server. Pentru acest lucru, va incepe procesul de restaurea a bazei de date.
        :return: Success - daca s-a facut restaurea bazei de date din aplicatie. Ready - daca restaurarea s-a facut in urma absentei
        bazei de date pe server
        """
        layout = []
        top_layout = [[sg.Text("Introduceti numele serverului: ", size=(20, 1)), sg.Input(key="-SERVER-", size=(35,1))]]
        layout_middle = [[sg.Text("Selectati fisierul de backup: ",size=(20, 1)), sg.InputText(key='-FILE_RESTORE_BACKUP-',size=(35,1)), sg.FileBrowse()]]
        layout_bottom = [[sg.Button("Restaureaza baza de date"), sg.Button("Anuleaza")]]
        if restore_database_notexists:
            layout += top_layout + layout_middle + layout_bottom
        else:
            layout += layout_middle + layout_bottom
        window = sg.Window(layout=layout, title="Restore database")
        while True:
            event, values = window.read()
            if event == "Restaureaza baza de date":
                file_path = values['-FILE_RESTORE_BACKUP-']
                if restore_database_notexists:
                    server_name = values['-SERVER-']
                    connection = connection_database(restore_database_notexists=True, server_name=server_name)
                    connection.autocommit = True
                    cursor = connection.cursor()
                    res = SQL_restore_database(file_path=file_path, cursor=cursor, restore=True)
                else:
                    connection.autocommit = True
                    cursor = connection.cursor()
                    res = SQL_restore_database(file_path=file_path, cursor=cursor)
                if res == "Success" and restore_database_notexists:
                    sg.popup("Restaurarea a fost creata cu succes!\nAplicatia se va inchide automat. Lansati programul din nou.", title="Restore database")
                    window.close()
                    return "Ready"
                elif res == "Success":
                    connection.autocommit = False
                    sg.popup("Restaurarea a fost creata cu succes!\nAplicatia se va inchide automat. Lansati programul din nou.", title="Restore database")
                    window.close()
                    return "Success"
                elif res == "Error":
                    sg.popup("A aparut o eroare in procesul de restaurare", title="Restore")
            if event == sg.WINDOW_CLOSED or event == "Anuleaza":
                window.close()
                if not restore_database_notexists: connection.autocommit = False
                return

    def update_window(self, cursor, data_for_update, index):
        """
        Functie responsabila de a modifica o inregistrare existenta din tabel.
        Se deschide o fereastra noua ce contine datele inregistrarii selectate spre modificare,
        si o data cu apasarea butonului modifica, datele sunt trimise catre baza de date pentru a fi modificate.
        :param cursor: type - cursor - cursorul conexiunii
        :param data_for_update: type list - lista care contine datele inregistrarii ce urmeaza a fi modificata
        :param index: type int - pozitia inregistrarii in tabelul SQL
        :return: Returneaza mesajul de Success, daca modificarea a fost executata cu succes, eroare altlfel.
        """
        middle = []
        j = 0
        for i in range(len(self.headings) // 2):
            if self.headings[j + 1] == "SORT_COUNT":
                middle += [[sg.Text(self.headings[j], size=(11, 1)), sg.Multiline(data_for_update[j],key=self.headings[j],size=(38,1),
                                                                                  background_color=TableDisplay.INPUT_BACKGROUND,
                                                                                  text_color=TableDisplay.TEXT_COLOR),
                            ]]
            else:
                middle += [[sg.Text(self.headings[j], size=(11, 1)), sg.Multiline(data_for_update[j], key=self.headings[j],size=(38,1),
                                                                                  background_color=TableDisplay.INPUT_BACKGROUND,
                                                                                  text_color=TableDisplay.TEXT_COLOR),
                            sg.Text(self.headings[j + 1], size=(11, 1)),
                            sg.Multiline(key=self.headings[j + 1], size=(38,1),
                                                                background_color=TableDisplay.INPUT_BACKGROUND,
                                                                text_color=TableDisplay.TEXT_COLOR)]]
            j += 2
        layout = [
            [sg.Text(" " * 60 + "Modifica inregistrarea", font=("Times New Roman", 15))],
            [sg.Text("_" * 300)],
            [sg.Column(middle, scrollable=True)],
            [sg.Text("_" * 300)],
            [sg.Button("Modifica"), sg.Button("Anuleaza")]
        ]
        window = sg.Window("Modifica", layout, size=(900, 550))
        while True:
            event, values = window.read()
            if event == "Modifica":
                res = SQL_update(data=list(values.items()), cursor=cursor, ndx = index)
                if res == "Success":
                    sg.popup("Inregistrarea a fost modificata cu succes!")
                    window.close()
                    break
            if event == "Anuleaza" or sg.WINDOW_CLOSED == event:
                window.close()
                return

    def refresh_data(self, cursor, autorefresh=False):
        """

        :param cursor: type cursor - Cursorul conexiunii
        :param autorefresh:  type boolean - Daca este True, se va executa functia dupa o alta comanda, iar daca este
        false, inseamna ca se va executa special, prin apelarea functiei.
        :return: returneaza mesaj de succes, in caz de succes, eroare altfel.
        """
        refresh_data = SQL_Select(cursor)
        if refresh_data == "Error":
            sg.popup("Nu s-a putut efectua reimprospatarea!", title="Refresh")
        else:
            if not autorefresh:
                sg.popup("Reimprospatarea s-a efectuat cu succes!", title="Refresh")
            return refresh_data

    def delete_window(self, entry, conn,multiple_values=False):
        """

        :param entry: type List(List), List   Inregistrarea/Inregistrarile ce urmeaza a fi sterse din tabel
        :param conn: cursorul
        :param multiple_values: Daca True, inseamna ca au fost mai multe date trimise spre stergere, flase altfel.
        :return:
        """
        if multiple_values:
            return SQL_delete(entry, conn, multiple_values=True)
        else:
            return SQL_delete(entry, conn)


    def window_tbl(self):
        """
        Functie responsabila de crearea ferestrei tabelului
        :return:
        """
        title = "Links    -   F2-Search    /    F3-Insert above    /    F4-Insert bellow " \
                "   /   F5-Update    /    F6-Delete    /    Enter KEY - Open     /     F7 OR R - Refresh    /   F8 - Reindexare     " \
                "-       Realizat de Tudor"
        window = sg.Window(title, self.layout, enable_close_attempted_event=True,
                           finalize=True, return_keyboard_events=True, size=(1200, 650), resizable=True,
                           )
        return window

def program_body(connection):
    cursor = connection.cursor()
    data = SQL_Select(cursor)
    app = TableDisplay(data)
    window = app.window_tbl()
    while True:
        event, values = window.read()
        if event in ('\r', app.QT_ENTER_KEY1):
            print("kjsnkndakn")
            if len(values['-TABEL-']) > 0:
                res = app.open_link(data[values['-TABEL-'][0]])
                if res == "invalid http":
                    sg.popup("Valoarea din tabel nu este un link valid (http-https)!")
        if event == "Backup":
            app.backup(connection)
        if event == "F2:113":
            result = app.search_window(data)
            window["-TABEL-"].update(values=result)
            if result is not None:
                sg.popup(f"S-au gasit {len(result)} rezultate!", title="Results")
        if event == "F3:114":
            if len(values['-TABEL-']) > 0:
                res = app.insert_window(sort_count=int(data[values['-TABEL-'][0]][-1]), cursor=cursor, up=True)
                if res == "Success":
                    data = app.refresh_data(cursor, autorefresh=True)
                    window['-TABEL-'].update(data)
                    sg.popup("O noua inregistrare a fost adaugata cu succes!")
                elif res == "Error":
                    sg.popup("A aparut o eroare", "Error")
        if event == "F4:115":
            if len(values['-TABEL-']) > 0:
                res = app.insert_window(sort_count=int(data[values['-TABEL-'][0]][-1]), cursor=cursor, down=True)
                if res == "Success":
                    data = app.refresh_data(cursor,  autorefresh=True)
                    window['-TABEL-'].update(data)
                    sg.popup("O noua inregistrare a fost adaugata cu succes!")
                elif res == "Error":
                    sg.popup("A aparut o eroare")
        if event == "F5:116":
            if len(values['-TABEL-']) > 0:
                app.update_window(data_for_update=data[values['-TABEL-'][0]], cursor=cursor, index=data[values["-TABEL-"][0]][-1])
                data = app.refresh_data(cursor,  autorefresh=True)
                window['-TABEL-'].update(data)
            else:
                sg.popup("Selectati randul dorit pentru a putea modifica o intrare!", title="Error")
        if event == "F6:117":
            if len(values["-TABEL-"]) > 0:
                if sg.popup_yes_no(
                        "Sunteti sigur ca vreti sa stergeti inregistrarea?\nActiunea va fi permanenta.") == "Yes":
                    if len(values['-TABEL-']) > 1:
                        entry = [data[i] for i in values['-TABEL-']]
                        res = app.delete_window(entry, cursor, multiple_values=True)
                        message = "Inregistrarile au fost sterse cu succes!"
                    else:
                        res = app.delete_window(data[values["-TABEL-"][0]], cursor)
                        message = "Inregistrarea a fost stearsa cu succes!"
                    if res == "Success":
                        data = app.refresh_data(cursor,  autorefresh=True)
                        window['-TABEL-'].update(data)
                        sg.popup(message)
                    else:
                        sg.popup("Inregistrarea nu a putut fi stearsa!")
        if (event == "F7:118" or event == "r") and sg.popup_yes_no("Doriti sa dati refresh?", title="Refresh") == "Yes":
            data = app.refresh_data(cursor)
            window['-TABEL-'].update(data)
        if (event == "F8:119") and sg.popup_yes_no("Doriti sa reindexati tabelele?", title="Reindexare") == "Yes":
            reindexare = SQL_reindexare(cursor)
            if reindexare == "Success":
                sg.popup("Indexarea s-a efectuat cu succes!", title="Reindexare")
                data = app.refresh_data(cursor,autorefresh=True)
                window['-TABEL-'].update(data)
            elif reindexare == "Error":
                sg.popup("Indexarea nu s-a putut efectua! A aparut o problema!", title="Reindexare")
        if ((event == sg.WIN_CLOSE_ATTEMPTED_EVENT or event == "Exit" or event == "Escape:27") and
                sg.popup_yes_no("Doriti sa inchideti aplicatia?") == "Yes"):
            break
        if event == "Restore database":
            res = app.restore_database(connection=connection)
            if not res != "Error":
                window.close()
                break
            elif res == "Success" or res == "Ready":
                window.close()
                break
        if event == "New table":
            pass



def main():
    connection = connection_database()
    if connection is None:
        sg.popup("Eroare la conexiunea cu baza de date!", title="Connection error")
    elif connection == "Restore database":
        sg.popup("Nu exista baza de date\nBaza de date necesita a fi restaurata", title="Connection error")
        res = TableDisplay.restore_database(restore_database_notexists=True)
        if res == "Ready":
            connection = connection_database()
            program_body(connection)
    else:
        program_body(connection)



if __name__ == "__main__":
    main()
